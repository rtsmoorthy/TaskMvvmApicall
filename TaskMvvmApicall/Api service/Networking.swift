//
//  Networking.swift
//  TaskMvvmApicall
//
//  Created by Ranjith on 16/12/2021.
//

import Foundation
import Alamofire

class Networking {
    var user = User()
    var params = [Welcome]()
    
    func get(id:String){
        AF.request(user.getUrl+id).response { response in
            debugPrint(response)
        }
    }
    func post(){
        AF.request(user.postUrl,
           method: .post,
           parameters: user.logindetails,
           encoder: JSONParameterEncoder.default).response { response in
            debugPrint(response)
}
}
    func put(){
        AF.request(user.putUrl,
            method: .put,
            parameters: user.logindetails,
            encoder: JSONParameterEncoder.default).response { response in
    debugPrint(response)
        }
    }
    func delete(){
         AF.request(user.deleteUrl,
               method: .delete).response { response in
    debugPrint(response)
           }
    }
}

