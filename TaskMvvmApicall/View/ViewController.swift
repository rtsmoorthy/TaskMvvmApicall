//
//  ViewController.swift
//  TaskMvvmApicall
//
//  Created by Ranjith on 16/12/2021.
//

import UIKit

class ViewController: UIViewController {
    var Http = Networking()
    
    @IBAction func getButton(_ sender: Any) {
        Http.get(id:"1")
    }
    @IBAction func postButton(_ sender: Any) {
        Http.post()
    }
    
    @IBAction func putButton(_ sender: Any) {
        Http.put()
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        Http.delete()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }


}

